using System;

namespace Task2
{		
	public class Program
	{
		public static void Main()
		{
			//Створення обєктів
			Rectangle rectangle1 = new Rectangle(1,1,6,3);
			Rectangle rectangle2 = new Rectangle(5,2,5,4);
			Rectangle result;
			
			//Вивід фігур з якими проводитимемо операції
			Console.WriteLine("1st rectangle: {0}", rectangle1);
			Console.WriteLine("2nd rectangle: {0}\n", rectangle2);
			
			Console.WriteLine("Operations rectangles");
			
			//Перетин першого і другого прямокутників
			result = Rectangle.Merge(rectangle1, rectangle2);
			Console.WriteLine("Union of this rectangle: {0}\n", result);
			
			//Перетин
			result = Rectangle.Intersect(rectangle1, rectangle2);
			Console.WriteLine("Intersect 1st & 2nd regtangles: {0}\n", result);
			
			//Переміщення першого прямокутника в задану точку		
			double x = 1, y = 3;
			rectangle1.Move(x, y);
			Console.WriteLine("Moved 1st rectangle in point [{0}; {1}]: {2}\n", x, y, rectangle1);
			
			//Зміна розмірів
			double length = 2, height = 5;
			rectangle2.Resize(length,height);
			Console.WriteLine("Large 2nd rectangle in length = {0}, height = {1}: {2}\n", length, height, rectangle2);
			
			//Перевірка рівності фігур
			Console.WriteLine("1st rectangle = 2nd rectangle: {0}", rectangle1 == rectangle2);
		}
	}

	public class Rectangle
	{
		#region Constructors
		public Rectangle(double x, double y, double width, double height) 
		{
			this.X = x;
			this.Y = y;
			this.Width = width; 
			this.Height = height;
		} 
		#endregion
		
		#region Properties	
		public double X { get; private set;}
		public double Y { get; private set;}
		public double Width { get; private set;}
		public double Height { get; private set;}
		public bool IsEmpty 
		{ 
			 get 
			 {
				return Height == 0 && Width == 0 && X == 0 && Y == 0; 
			 }
		} 
		#endregion
		
		#region Methods
		//Union of 2 rectangles
		public static Rectangle Merge(Rectangle a, Rectangle b) 
		{ 
			double x1 = Math.Min(a.X, b.X);
			double x2 = Math.Max(a.X + a.Width, b.X + b.Width); 
			double y1 = Math.Min(a.Y, b.Y);
			double y2 = Math.Max(a.Y + a.Height, b.Y + b.Height);
	 
			return new Rectangle(x1, y1, x2 - x1, y2 - y1); 
		}
		
		//Intersect of 2 rectangles
		public static Rectangle Intersect(Rectangle a, Rectangle b)
		{
			double x1 = Math.Max(a.X, b.X);
			double x2 = Math.Min(a.X + a.Width, b.X + b.Width); 
			double y1 = Math.Max(a.Y, b.Y);
			double y2 = Math.Min(a.Y + a.Height, b.Y + b.Height); 
	  
			if (x2 >= x1 && y2 >= y1) 
			{ 
				return new Rectangle(x1, y1, x2 - x1, y2 - y1);
			}
			
			return null; 
		}
		
		public override string ToString()
		{ 
			return String.Format("x = {0}, y = {1}, Width = {2}, Height = {3}", this.X, this.Y, this.Width, this.Height);	   
		}
		
		public override int GetHashCode()
		{
			return base.GetHashCode();
		}
		
		public override bool Equals(object obj) 
		{ 
			if (!(obj is Rectangle)) return false; 
	  
			Rectangle comp = (Rectangle)obj;
	  
			return (comp.X == this.X) &&
				   (comp.Y == this.Y) &&
				   (comp.Width == this.Width) &&
				   (comp.Height == this.Height); 
		}
		
		//To move rectangle in new point
		public void Move(double x1, double y1)
		{
			this.X = x1;
			this.Y = y1;
		}
		
		//Resize rectangle
		public void Resize(double length, double height)
		{
			this.Width += length;
			this.Height += height;
		}
		
		#endregion
		
		#region Operators
		//Equal of 2 rectangles
		public static bool operator ==(Rectangle fRectangle, Rectangle sRectangle) 
		{
			return (fRectangle.X == sRectangle.X 
					&& fRectangle.Y == sRectangle.Y 
					&& fRectangle.Width == sRectangle.Width
					&& fRectangle.Height == sRectangle.Height); 
		}
	 		   
		public static bool operator !=(Rectangle fRectangle, Rectangle sRectangle)
		{
			return !(fRectangle == sRectangle);
		}
		#endregion	
	}
}